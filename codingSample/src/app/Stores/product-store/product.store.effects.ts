import { ProductDefinition } from './../../Models/productDefinitionModel';
import { Product } from './../../Models/productModel';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Actions, createEffect, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { map, mergeMap, catchError, switchMap } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import * as FormProductAction from './product.store.action';

@Injectable()
export class ProductsEffaces {
    constructor(private actions$: Actions, private httpOperation: HttpClient) {}
    @Effect()
    getProducts$ = this.actions$.pipe(
        ofType(FormProductAction.startGetProductList),
        mergeMap(() => {
            return this.httpOperation
                .get<Product[]>(environment.getProductsApiUrl)
                .pipe(
                    map((res) => {
                        return FormProductAction.successGetProductList({
                            payload: res,
                        });
                    }),
                    catchError((err) => {
                        return of(
                            FormProductAction.failGetProductList({
                                payload: 'Unable to get Products.',
                            })
                        );
                    })
                );
        })
    );

    @Effect()
    getProductsDefinitions$ = this.actions$.pipe(
        ofType(FormProductAction.startGetProductDefinitions),
        switchMap((data) => {
            return this.httpOperation
                .get<ProductDefinition[]>(data.payload)
                .pipe(
                    map((res) => {
                        return FormProductAction.successGetProductDefinitions({
                            payload: res,
                        });
                    }),
                    catchError((err) => {
                        return of(
                            FormProductAction.failGetProductDefinitions({
                                payload: 'Unable to get product definitions.',
                            })
                        );
                    })
                );
        })
    );
}
