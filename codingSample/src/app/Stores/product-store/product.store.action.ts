import { ProductDefinition } from './../../Models/productDefinitionModel';
import { createAction, props } from '@ngrx/store';
import { Product } from 'src/app/Models/productModel';

export const startGetProductList = createAction(
    '[Product] startGetProductList'
);

export const successGetProductList = createAction(
    '[Product] successGetProductList',
    props<{ payload: Product[] }>()
);

export const failGetProductList = createAction(
    '[Product] failGetProductList',
    props<{ payload: string }>()
);

export const startGetProductDefinitions = createAction(
    '[Product] startGetProductDefinitions',
    props<{ payload: string }>()
);

export const successGetProductDefinitions = createAction(
    '[Product] successGetProductDefinitions',
    props<{ payload: ProductDefinition[] }>()
);

export const failGetProductDefinitions = createAction(
    '[Product] FailGetProductDefinitions',
    props<{ payload: string }>()
);
