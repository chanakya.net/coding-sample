import { ProductDefinition } from './../../Models/productDefinitionModel';
import { Product } from './../../Models/productModel';
import { Action, ActionReducer, createReducer, on, State } from '@ngrx/store';
import * as FromProductActions from './product.store.action';

export interface ProductState {
    productList: Product[];
    errorMessage: string;
    productDefinitionList: ProductDefinition[];
    productDefinitionUrl: string;
    isRequestInProgress: boolean;
}

export const InitialState: ProductState = {
    productList: [],
    errorMessage: '',
    productDefinitionList: [],
    productDefinitionUrl: '',
    isRequestInProgress: false,
};

const getProductReducers = createReducer(
    InitialState,
    on(FromProductActions.startGetProductList, (state) => ({
        ...state,
        productList: [],
        errorMessage: '',
        productDefinitionList: [],
        productDefinitionUrl: '',
        isRequestInProgress: true,
    })),
    on(FromProductActions.successGetProductList, (state, { payload }) => ({
        ...state,
        errorMessage: '',
        productList: payload,
        productDefinitionList: [],
        productDefinitionUrl: '',
        isRequestInProgress: false,
    })),
    on(FromProductActions.failGetProductList, (state, { payload }) => ({
        ...state,
        productList: [],
        errorMessage: payload,
        productDefinitionList: [],
        productDefinitionUrl: '',
        isRequestInProgress: false,
    })),
    on(FromProductActions.startGetProductDefinitions, (state, { payload }) => ({
        ...state,
        errorMessage: '',
        productDefinitionList: [],
        productDefinitionUrl: payload,
        isRequestInProgress: true,
    })),
    on(FromProductActions.failGetProductDefinitions, (state, { payload }) => ({
        ...state,
        errorMessage: payload,
        productDefinitionList: [],
        productDefinitionUrl: '',
        isRequestInProgress: false,
    })),
    on(
        FromProductActions.successGetProductDefinitions,
        (state, { payload }) => ({
            ...state,
            errorMessage: '',
            productDefinitionList: payload,
            productDefinitionUrl: '',
            isRequestInProgress: false,
        })
    )
);

export function reducer(state: ProductState | undefined, action: Action) {
    return getProductReducers(state, action);
}
