import { ProductBase } from './../dynamic-form/product-base';
import { Observable } from 'rxjs';
import { ProductDefinition } from './../Models/productDefinitionModel';
import { Component, Input, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ProductState } from '../Stores/product-store/product.store.reducers';
import { ProductControlService } from '../dynamic-form/product-control-service';
import { FormGroup } from '@angular/forms';
import { CreateControlService } from '../dynamic-form/create-controls-service';

@Component({
    selector: 'cs-product-definitions',
    templateUrl: './product-definitions.component.html',
    styleUrls: ['./product-definitions.component.scss'],
})
export class ProductDefinitionsComponent implements OnInit {
    productDefinitions$ = new Observable<ProductDefinition[]>();
    productsDetails: ProductBase<string>[] = [];
    form!: FormGroup;
    payLoad = '';
    constructor(
        private store$: Store<{ ProductState: ProductState }>,
        public productsControlService: ProductControlService,
        public controlService: CreateControlService
    ) {}

    ngOnInit(): void {
        this.productDefinitions$ = this.store$.select(
            (pd) => pd.ProductState.productDefinitionList
        );

        this.store$
            .select((pd) => pd.ProductState.productDefinitionList)
            .subscribe((data) => {
                if (data) {
                    this.productsDetails = this.controlService.getAllControls(
                        data
                    );
                    this.form = this.productsControlService.toFormGroup(
                        this.controlService.getAllControls(data)
                    );
                }
            });
    }

    onSubmit() {
        this.payLoad = JSON.stringify(this.form.getRawValue());
    }
}
