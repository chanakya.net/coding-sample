import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductDefinitionsComponent } from './product-definitions.component';

describe('ProductDefinitionsComponent', () => {
  let component: ProductDefinitionsComponent;
  let fixture: ComponentFixture<ProductDefinitionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductDefinitionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductDefinitionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
