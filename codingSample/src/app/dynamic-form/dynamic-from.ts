import { ProductBase } from './product-base';
import { Component, Input } from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
    selector: 'cs-product-control',
    templateUrl: 'dynamic-form.html',
    styleUrls: ['./dynamic-from.scss'],
})
export class DynamicFormQuestionComponent {
    @Input() productsData!: ProductBase<string>;
    @Input() form!: FormGroup;
    get isValid() {
        return this.form.controls[this.productsData.key].valid;
    }
}
