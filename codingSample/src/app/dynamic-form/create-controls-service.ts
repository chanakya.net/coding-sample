import { ProductDefinition } from './../Models/productDefinitionModel';
import { Injectable } from '@angular/core';
import { ProductBase } from './product-base';
import { ProductText } from './product-text';
import { productDropDown } from './product-dropdown';
import { of, Observable } from 'rxjs';

@Injectable()
export class CreateControlService {
    getAllControls(productDetails: ProductDefinition[]): ProductBase<string>[] {
        const productDefinitionsControls: ProductBase<string>[] = [];
        productDetails.forEach((p) => {
            if (p.type === 'text' || p.type === 'int') {
                const tempstore = new ProductText({
                    key: p.caption,
                    label: p.caption,
                    value: p.defaultValue,
                    required: p.mandatory,
                    validationMessage: p.validationMessage,
                });
                productDefinitionsControls.push(tempstore);
                // create text
            } else if (p.type === 'bool') {
                const ddp = new productDropDown({
                    key: p.caption,
                    label: p.caption,
                    required: p.mandatory,
                    validationMessage: p.validationMessage,
                    options: [
                        {
                            key: 'Yes',
                            value: 'Yes',
                        },
                        { key: 'No', value: 'No' },
                    ],
                });
                productDefinitionsControls.push(ddp);
            }
        });
        return productDefinitionsControls;
    }
}
