export class ProductBase<T> {
    value: T | undefined;
    key: string;
    label: string;
    required: boolean;
    controlType: string;
    type: string;
    validationMessage: string;
    options: { key: string; value: string }[];

    constructor(
        options: {
            value?: T;
            key?: string;
            label?: string;
            required?: boolean;
            controlType?: string;
            type?: string;
            validationMessage?: string;
            options?: { key: string; value: string }[];
        } = {}
    ) {
        this.value = options.value;
        this.key = options.key || '';
        this.label = options.label || '';
        this.required = !!options.required;
        this.controlType = options.controlType || '';
        this.type = options.type || '';
        this.validationMessage = options.validationMessage || '';
        this.options = options.options || [];
    }
}
