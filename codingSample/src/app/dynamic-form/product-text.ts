import { ProductBase } from './product-base';

export class ProductText extends ProductBase<string> {
    controlType = 'textbox';
}
