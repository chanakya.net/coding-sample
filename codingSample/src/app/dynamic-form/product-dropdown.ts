import { ProductBase } from './product-base';

export class productDropDown extends ProductBase<string> {
    controlType = 'dropdown';
}
