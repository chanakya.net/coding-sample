import { ProductBase } from './product-base';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Injectable()
export class ProductControlService {
    constructor() {}
    toFormGroup(productsDetails: ProductBase<string>[]) {
        const group: any = {};

        productsDetails.forEach((productsDetail) => {
            group[productsDetail.key] = productsDetail.required
                ? new FormControl(
                      productsDetail.value || '',
                      Validators.required
                  )
                : new FormControl(productsDetail.value || '');
        });
        return new FormGroup(group);
    }
}
