export class ProductDefinition {
    caption!: string;
    type!: string;
    mandatory!: boolean;
    defaultValue!: string;
    validationMessage!: string;
}
