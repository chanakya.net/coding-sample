import { FormGroup } from '@angular/forms';
import { ProductDefinition } from './productDefinitionModel';

export interface Field {
    config: ProductDefinition;
    group: FormGroup;
}
