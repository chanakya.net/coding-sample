import { ProductDefinition } from './Models/productDefinitionModel';
import { Product } from 'src/app/Models/productModel';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import * as FromProductActions from './Stores/product-store/product.store.action';
import { ProductState } from './Stores/product-store/product.store.reducers';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
    selector: 'cs-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
    title = 'codingSample';
    productList$ = new Observable<Product[]>();
    errorMessage$ = new Observable<string>();
    productDefinitions$ = new Observable<ProductDefinition[]>();
    isRequestInProgress$ = new Observable<boolean>();

    constructor(
        private store$: Store<{ ProductState: ProductState }>,
        private router: Router,
        private route: ActivatedRoute
    ) {}

    ngOnInit(): void {
        this.store$.dispatch(FromProductActions.startGetProductList());
        this.productList$ = this.store$.select(
            (x) => x.ProductState.productList
        );
        this.errorMessage$ = this.store$.select(
            (e) => e.ProductState.errorMessage
        );
        this.isRequestInProgress$ = this.store$.select(
            (x) => x.ProductState.isRequestInProgress
        );
    }

    getProductDefinitions(definitionUrl: string): void {
        this.store$.dispatch(
            FromProductActions.startGetProductDefinitions({
                payload: definitionUrl,
            })
        );
        this.router.navigate(['pd']);
    }
}
