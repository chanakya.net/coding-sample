import { Product } from './Models/productModel';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { reducer } from './Stores/product-store/product.store.reducers';
import { EffectsModule } from '@ngrx/effects';
import { ProductsEffaces } from './Stores/product-store/product.store.effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from 'src/environments/environment';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatButtonModule } from '@angular/material/button';
import { ProductDefinitionsComponent } from './product-definitions/product-definitions.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoadingComponent } from './loading/loading.component';
import { DynamicFormQuestionComponent } from './dynamic-form/dynamic-from';
import { ProductControlService } from './dynamic-form/product-control-service';
import { CreateControlService } from './dynamic-form/create-controls-service';

@NgModule({
    declarations: [
        AppComponent,
        ProductDefinitionsComponent,
        LoadingComponent,
        DynamicFormQuestionComponent,
    ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        HttpClientModule,
        ReactiveFormsModule,
        StoreModule.forRoot({ ProductState: reducer }),
        EffectsModule.forRoot([ProductsEffaces]),
        StoreDevtoolsModule.instrument({
            maxAge: 25, // Retains last 25 states
            logOnly: environment.production, // Restrict extension to log-only mode
        }),
        BrowserAnimationsModule,
        MatButtonModule,
    ],
    providers: [ProductControlService, CreateControlService],
    bootstrap: [AppComponent],
})
export class AppModule {}
